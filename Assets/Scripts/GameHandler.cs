﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    public int trust = 0;
    public int mood = 0;
    public int motivation = 0;

    [Space(10)]
    public int baseTrust = 0;
    public int baseMood = 0;
    public int baseMotivation = 0;

    [Space(10)]
    public int social = 0;
    public int knowlege = 0;
    public int family = 0;




    [Space(10)]
    public LifeEventsHandler lifeEventsHandler;
    [Space(10)]
    public TextPanel textPanel;
    [Space(10)]
    public GameObject endgamePanel;
    public TMPro.TextMeshProUGUI endgameText;

    public Button exitgameButton;

    private Player player;
    private static GameHandler instance;

    public static GameHandler Instance
    {
        get
        {
            return instance;
        }
    }


    private void Awake()
    {
        if (instance == null) instance = this;
        //Debug.Log("Game Starting in 2");
        //StartCoroutine(DoAfter(() => 
        //{
        //    OnGameStartButtonPressed();
        //}, 2));
        player = GameObject.FindObjectOfType<Player>();

        int[] randomBase = { UnityEngine.Random.Range(-1,2) , UnityEngine.Random.Range(-1, 2), UnityEngine.Random.Range(-1, 2) };
        //randomBase.Shuffle();
        baseTrust = randomBase[0];
        baseMood = randomBase[1];
        baseMotivation = randomBase[2];
        lifeEventsHandler.lifeEvents.Shuffle();


        if (exitgameButton != null) exitgameButton.onClick.AddListener(Application.Quit);
    }

    public void OnGameStartButtonPressed()
    {
        //Do this
        PlayIntro();
    }

    private void PlayIntro()
    {
        Debug.Log("Intro Started");
        if (player != null) player.PlayIntro(OnIntroEnded);
        else
        {
            StartCoroutine(DoAfter(OnIntroEnded, 2));
        }
    }

    private void OnIntroEnded()
    {
        //Play First Event in 3 seconds
        Debug.Log("Intro Ended");
        Debug.Log("Playing First Event");
        lifeEventsHandler.PlayNext();

    }


    public void LifeEventChoiceChosen(LifeEventChoice choice)
    {
        

        if(choice.isNull == false)
        {
            trust = baseTrust + choice.trustModifier;
            mood = baseMood + choice.moodModifier;
            motivation = baseMotivation + choice.motivationModifier;

            lifeEventsHandler.currentLifeEvent.trustDecision = trust;
            lifeEventsHandler.currentLifeEvent.moodDecision = mood;
            lifeEventsHandler.currentLifeEvent.motivationDecision = motivation;
            lifeEventsHandler.currentLifeEvent.selectedChoiceTexts = choice.choiceTexts;
        }

        lifeEventsHandler.PlayNext();
    }

    public void OnEventsEnd()
    {
        player.gameObject.SetActive(false);

        string finalText = "Congratulations! You have successfully guided Thomas Alva Edison throughout his journey of lighting up the first light bulb! Thanks to your help, Edison";
        //For Now
        if (knowlege > 0)
        {
            finalText += " now knows 10.000 ways that a light bulb doesn't light.\n";
        }
        else if (knowlege > -1)
        {
            finalText += " now knows that there's no substitute for hard work.\n";
        }
        else
        {
            finalText += " now knows that he was merely wasting his time, because one light bulb won't decide his future.\n";
        }

        if (social > 1)
        {
            finalText += "Edison is a popular man, surrounded by friends and admirers.\n";
        }
        else if (social > 0)
        {
            finalText += "Edison is happy spending time both alone and with his close friends.\n";
        }
        else
        {
            finalText += "Edison is an introvert and likes spending most of his time alone.\n";
        }

        

        if (family > 0)
        {
            finalText += "Overall, he is grateful that you helped him realize how close he was to success and encouraged him to not give up\n";
        }
        else if (family > -1)
        {
            finalText += "Overall, he is grateful that you helped him hustle while he was waiting and encouraged him to try one more time\n";
        }
        else
        {
            finalText += "Overall, he believes in himself and knows that if he did all the things he is capable of, he would literally astound himself.\n";
        }

        endgameText.text = finalText;

        endgamePanel.transform.localScale = Vector3.zero;
        endgamePanel.SetActive(true);
        endgamePanel.LeanScale(Vector3.one, 1);

        if (exitgameButton != null) exitgameButton.gameObject.SetActive(true);
    }

    public void OnRestartButtonPressed()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public IEnumerator DoAfter(System.Action callback, float time = 0.1f)
    {
        yield return new WaitForSeconds(time);
        callback?.Invoke();
    }
}
