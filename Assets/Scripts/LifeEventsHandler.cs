﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeEventsHandler : MonoBehaviour
{
 
    [SerializeField]
    public List<LifeEvent> lifeEvents;
    private List<GameObject> eventButtons;
    public Pooler lifeEventChoiceButtonsPool;

    public CountDowner timerObject;

    public GameObject buttonsPanel;

    public LifeEvent currentLifeEvent;

    private void SetLifeEvent()
    {
        Debug.Log("Set LE");
        if(lifeEvents.Count > 0)
        {
            currentLifeEvent = lifeEvents[0];
            lifeEvents.RemoveAt(0);

            currentLifeEvent.handler = this;
            currentLifeEvent.OnEventIntroEnded = OnLifeEventIntroEnded;
            currentLifeEvent.OnEventEndingEnded = OnLifeEventEndingEnded;

            LifeEventChoiceButton button;
            eventButtons = new List<GameObject>();
            if (currentLifeEvent.playerGivenChoices.Length > 0)
            {
                for (int i = 0; i < currentLifeEvent.playerGivenChoices.Length; i++)
                {
                    button = lifeEventChoiceButtonsPool.Get().GetComponent<LifeEventChoiceButton>();
                    button.SetValues(currentLifeEvent.playerGivenChoices[i]);
                    Debug.Log("currentLifeEvent.playerGivenChoices[i] " + currentLifeEvent.playerGivenChoices[i]);
                    eventButtons.Add(button.gameObject);
                }
                
            }
            else
            {
                Debug.Log("No Choices in Life Event, playing next");
                PlayNext();

                return;
            }

            
            currentLifeEvent.PlayLifeEvent();
        }
        else
        {
            //Game Ended
            GameHandler.Instance.OnEventsEnd();
        }
    }

    public void OnLifeEventIntroEnded()
    {
        buttonsPanel.SetActive(true);
        for (int i = 0; i < eventButtons.Count; i++)
        {
            eventButtons[i].SetActive(true);
        }
        timerObject.StartCountingDown(15, OnTimeUp);
    }

    public void OnLifeEventEndingEnded()
    {
        SetLifeEvent();
    }

    public void PlayNext()
    {
        buttonsPanel.SetActive(false);

        Debug.Log("Play Next Event Called");
        if (currentLifeEvent != null)
        {
            currentLifeEvent.EndLifeEvent();
            timerObject.Stop();
            lifeEventChoiceButtonsPool.SetAllOff();
        }
        else
        {
            SetLifeEvent();
        }
    }

    private void OnTimeUp()
    {
        GameHandler.Instance.LifeEventChoiceChosen(currentLifeEvent.noChoiceSelectedChoice);
        buttonsPanel.SetActive(false);
        lifeEventChoiceButtonsPool.SetAllOff();
    }

    public IEnumerator DoAfter(System.Action callback, float time = 0.1f)
    {
        yield return new WaitForSeconds(time);
        callback?.Invoke();
    }
}
