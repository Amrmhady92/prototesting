﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CountDowner : MonoBehaviour
{
    public GameObject timerObject;
    public Image loadingImage;

    public void StartCountingDown(float time = 1, System.Action callback = null)
    {
        LeanTween.cancel(loadingImage.gameObject);
        loadingImage.fillAmount = 1;
        timerObject.SetActive(true);

        LeanTween.value(loadingImage.gameObject, 1, 0, time).setOnUpdate((float v) => { loadingImage.fillAmount = v; }).setOnComplete(()=>{ callback?.Invoke(); });
    }

    public void Stop()
    {
        timerObject.SetActive(false);
        LeanTween.cancel(loadingImage.gameObject);
    }


}
