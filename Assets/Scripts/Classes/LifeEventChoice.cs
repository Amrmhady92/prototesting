﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


//public enum LifeChoiceType
//{
//    Agree,
//    Disagree,
//    NeitherAgreeNorDisagree,
//    DoesntCare
//}

[System.Serializable]
public class ChoiceTexts
{
    public string selectedText = "Selected";
    public string failAllText= "Fail All";

    public string trustFailText = "Trust Pass"; // Should be Trust Pass text not fail
    public string moodFailText = "Mood Pass";
    public string motivationFailText = "Motivation Pass";

    public string GetText(bool passed, bool trustPass, bool moodPass, bool motivationPass)
    {
        if (passed) return selectedText;
        if (!trustPass && !moodPass && !motivationPass) return failAllText;
        else if (trustPass) return trustFailText;
        else if (moodPass) return moodFailText;
        else if (motivationPass) return motivationFailText;

        return "";
    }
}
[System.Serializable]
public class LifeEventChoice
{

    public string eventText = "Life Event Choice Empty";
    public bool isSprite = false;
    public Sprite sprite;

    public bool isNull = false;
    public bool isRandom = false;

    public int trustModifier = 0;
    public int moodModifier = 0;
    public int motivationModifier = 0;


    public ChoiceTexts choiceTexts;
}
