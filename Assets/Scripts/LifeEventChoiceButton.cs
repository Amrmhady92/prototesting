﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
public class LifeEventChoiceButton : MonoBehaviour
{

    public LifeEventChoice currentChoice;

    private static Action OnButtonPressedAction;
    [SerializeField] private TextMeshProUGUI eventText;
    [SerializeField] private Button choiceButton;
    [SerializeField] private Image image;

    public Button ChoiceButton
    {
        get
        {
            if (choiceButton == null) choiceButton = this.GetComponent<Button>();
            return choiceButton;
        }
    }
    public TextMeshProUGUI EventText
    {
        get
        {
            if (eventText == null) eventText = this.GetComponentInChildren<TextMeshProUGUI>();
            return eventText;
        }
    }

    public Image ImageRenderer
    {
        get
        {
            if (image == null) image = this.GetComponentInChildren<Image>();
            return image;
        }
    }

    private void Awake()
    {
        OnButtonPressedAction += ButtonPressedFollowUps;

    }

    public void SetValues(LifeEventChoice choice)
    {
        if (choice == null)
        {
            Debug.LogError("No choice passed");
            return;
        }

        
        currentChoice = choice;
        Debug.Log(currentChoice);

        if (choice.isSprite && choice.sprite != null && ImageRenderer != null)
        {
            ImageRenderer.gameObject.SetActive(true);
            ImageRenderer.sprite = choice.sprite;

            if (EventText != null) EventText.gameObject.SetActive(false);
        }
        else
        {
            if(EventText != null)
            {
                EventText.gameObject.SetActive(true);
                EventText.text = choice.eventText;
            }
            if (ImageRenderer != null) ImageRenderer.gameObject.SetActive(false);
        }
        
        ChoiceButton.interactable = true;
    }

    public void OnChoiceButtonPressed()
    {
        GameHandler.Instance.LifeEventChoiceChosen(currentChoice);

        //OnButtonPressedAction.Invoke();
    }


    private void ButtonPressedFollowUps()
    {

        ChoiceButton.interactable = false;
        currentChoice = null;
    }
}
