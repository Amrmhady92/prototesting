﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float limits = 1;
    public float oscillationSpeed = 1;
    public float smoothingSpeed = 5;
    public float shakeLimits = 1;
    public Vector3 shift;

    private bool active = false;
    public void PlayIntro(System.Action callback = null)
    {
        this.transform.LeanMove(Vector3.zero + shift, 4).setOnComplete(() => 
        {
            StartCoroutine(DoAfter(()=> 
            {
                this.transform.LeanScale(Vector3.one, 3).setOnComplete(() =>
                {
                    StartCoroutine(DoAfter(() =>
                    {
                        Debug.Log("Intro Ended");
                        active = true;
                        callback?.Invoke();
                    }
                    , 1));
                });
            } 
            , 1));
        });
    }


    Vector3 pos;
    private void Update()
    {
        if (!active) return;

        pos.y = Mathf.Sin(Time.time * oscillationSpeed) * limits;
        transform.position = Vector3.Lerp(transform.position, pos + shift, Time.deltaTime * smoothingSpeed);
    }

    public IEnumerator DoAfter(System.Action callback, float time = 0.1f)
    {
        yield return new WaitForSeconds(time);
        callback?.Invoke();
    }

    int side = 1;
    int counts = 4;
    internal void ShakeNo()
    {
        active = false;
        
        transform.LeanMove(transform.position + new Vector3(shakeLimits * side ,0,0), 0.2f).setOnComplete(()=> 
        {
            if(counts > 0)
            {
                counts--;
                side *= -1;
                ShakeNo();
            }
            else
            {
                active = true;
                counts = 4;
            }
        });
    }
}
