﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TextPanel : MonoBehaviour
{
    public float offY = -1000;
    public float onY = 1000;
    public float speed = 1;
    [SerializeField] private TextMeshProUGUI textbox;
    private RectTransform rectTransform;
    public string PanelText
    {
        set
        {
            textbox.text = value;
        }
    }

    public RectTransform PanelRectTransform
    {
        get
        {
            if (rectTransform == null) rectTransform = this.GetComponent<RectTransform>();
            return rectTransform;
        }
    }

    float remainingDistance;
    float totalDistance;
    float time;
    public void SetPanel(bool onoff = false)
    {
        if (PanelRectTransform == null)
        {
            Debug.LogError("No Rect Transform");
            return;
        }

        if (onoff) PanelRectTransform.LeanMoveY(offY, 0);

        LeanTween.cancel(PanelRectTransform);

        //totalDistance = Mathf.Abs(offY - onY);
        //remainingDistance = Mathf.Abs(PanelRectTransform.position.y - (onoff == true ? onY : offY));
        //time = remainingDistance * speed / totalDistance;


        PanelRectTransform.LeanMoveY(onoff == true ? onY : offY, speed);
    }


    bool testOnOff = false;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SetPanel(testOnOff);
            testOnOff = !testOnOff;
        }
    }
}
