﻿using System;
using UnityEngine;

[Serializable]
public abstract class LifeEvent : MonoBehaviour
{
    public string description = "Life Event";
    [Space(10)]
    [HideInInspector] public LifeEventsHandler handler;
    public LifeEventChoice[] playerGivenChoices;
    [Space(10)]
    public LifeEventChoice noChoiceSelectedChoice;

    //public bool isNull = false;

    public Action OnEventIntroEnded;
    public Action OnEventEndingEnded;


    [Space(10)]
    public int minRequiredTrust = -2;
    public int minRequiredMood = -2;
    public int minRequiredMotivation = -2;

    [HideInInspector] public int trustDecision = 0;
    [HideInInspector] public int moodDecision = 0;
    [HideInInspector] public int motivationDecision = 0;
    [HideInInspector] public ChoiceTexts selectedChoiceTexts;


    [Space(10)]
    public int familyModifier = 0;
    public int socialModifier = 0;
    public int knowlegeModifier = 0;

    public abstract void PlayLifeEvent();
    public abstract void EndLifeEvent();



}
