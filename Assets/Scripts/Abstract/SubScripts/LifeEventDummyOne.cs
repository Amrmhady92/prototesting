﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class LifeEventDummyOne : LifeEvent
{
    [Space(10)]
    public Vector3 onPosition;
    public Vector3 offPosition;
    [Space(10)]
    public Player player;
    [Space(10)]
    public GameObject eventSceneSpriteObject;
    public SpriteRenderer eventSpriteRenderer;
    public Sprite sprite;
    public float spriteScaleModifier = 0.4f;

    public override void PlayLifeEvent()
    {

        if (eventSpriteRenderer != null && sprite != null)
        {
            eventSpriteRenderer.sprite = sprite;
            eventSpriteRenderer.transform.localScale = Vector3.one * spriteScaleModifier;
        }


        eventSceneSpriteObject.transform.localScale = Vector3.one;
        Vector3 shift = new Vector3(UnityEngine.Random.Range(-1.5f, 1.5f), 0, 0);
        eventSceneSpriteObject.transform.position = offPosition + shift;
        eventSceneSpriteObject.transform.LeanMove(onPosition + shift, 5).setOnComplete(()=> 
        {
            OnEventIntroEnded.Invoke();
        });
    }


    public override void EndLifeEvent()
    {
        eventSceneSpriteObject.LeanCancel();

        bool choiceSelected = 
            trustDecision >= minRequiredTrust && 
            moodDecision >= minRequiredMood && 
            motivationDecision >= minRequiredMotivation;

        bool passTrust = false;
        bool passMood = false;
        bool passMotivation = false;

        if (trustDecision > 0) passTrust = trustDecision >= minRequiredTrust ? true : false;
        else if(trustDecision < 0) passTrust = trustDecision <= minRequiredTrust ? true : false;

        if (moodDecision > 0) passMood = moodDecision >= minRequiredMood ? true : false;
        else if (moodDecision < 0) passMood = moodDecision <= minRequiredMood ? true : false;

        if (motivationDecision > 0) passMotivation = motivationDecision >= minRequiredMotivation ? true : false;
        else if (motivationDecision < 0) passMotivation = motivationDecision <= minRequiredMotivation ? true : false;


        choiceSelected = 
             (minRequiredTrust == 0 || passTrust) &&
             (minRequiredMood == 0 || passMood) && 
             (minRequiredMotivation == 0 || passMotivation);

        GameHandler.Instance.textPanel.PanelText = selectedChoiceTexts.GetText(choiceSelected, (minRequiredTrust != 0 && passTrust), (minRequiredMood != 0 && passMood), (minRequiredMotivation != 0 && passMotivation));

        if (choiceSelected)
        {
            GameHandler.Instance.textPanel.SetPanel(true);
            eventSceneSpriteObject.transform.LeanMove(player.transform.position, 2);
            StartCoroutine(DoAfter(() =>
            {
                Debug.Log("Ending Ended");
                GameHandler.Instance.textPanel.SetPanel(false);
                OnEventEndingEnded.Invoke();
            }, 6));


            eventSceneSpriteObject.transform.LeanScale(Vector3.one * 0.1f, 3).setOnComplete(()=> 
            {
                eventSceneSpriteObject.transform.position = offPosition;
            });

            GameHandler.Instance.knowlege += knowlegeModifier;
            GameHandler.Instance.social += socialModifier;
            GameHandler.Instance.family += familyModifier;
        }
        else
        {
            GameHandler.Instance.textPanel.SetPanel(true);
            player.ShakeNo();
            eventSceneSpriteObject.transform.LeanMove(offPosition, 1);

            StartCoroutine(DoAfter(() =>
            {
                Debug.Log("Ending Ended");
                GameHandler.Instance.textPanel.SetPanel(false);
                OnEventEndingEnded.Invoke();
            }, 6));
        }


    }


    public IEnumerator DoAfter(System.Action callback, float time = 0.1f)
    {
        yield return new WaitForSeconds(time);
        callback?.Invoke();
    }

}
